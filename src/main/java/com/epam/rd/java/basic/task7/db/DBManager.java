package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Queue;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static final String APP_CONFIG_PATH = "app.properties";
	private static final String CONNECTION_URL = "connection.url";
	private final String URL;

	private static DBManager instance;

	public static synchronized DBManager getInstance() {
		if (instance == null) instance = new DBManager();
		return instance;
	}

	private DBManager() {
		URL = createProperties().getProperty(CONNECTION_URL);
	}


	public List<User> findAllUsers() throws DBException {
List<User> users = new ArrayList<>();
try(Connection connection = DriverManager.getConnection(URL);
	Statement statement = connection.createStatement();
	ResultSet rs = statement.executeQuery("SELECT * FROM users ");){
    while (rs.next()){
    	User user = new User();
    	user.setId(rs.getInt("id"));
    	user.setLogin(rs.getString("login"));
    	users.add(user);
	}

} catch (SQLException e){
	e.printStackTrace();
	throw new DBException("exception in findAllUsers", e);
}
		return users;
	}

	public boolean insertUser(User user) throws DBException {
		Connection connection = null;
		PreparedStatement statement = null;

		try {

			connection = DriverManager.getConnection(URL);
			statement = connection.prepareStatement("INSERT INTO users VALUES (DEFAULT, ?)", Statement.RETURN_GENERATED_KEYS);

			statement.setString(1, user.getLogin());

			int count = statement.executeUpdate();
			if(count > 0) {
				ResultSet rs = statement.getGeneratedKeys();
					if(rs.next()){
						user.setId(rs.getInt(1));
						return true;
					}
				}
			return false;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("exception in insertUser", e);
		} finally {
			helperClose(statement);
			helperClose(connection);
		}
	}

    private void helperClose (AutoCloseable stmt){
		if(stmt != null){
			try {
				stmt.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public boolean deleteUsers(User... users) throws DBException {
		try(Connection connection = DriverManager.getConnection(URL);
			PreparedStatement statement = connection.prepareStatement(" DELETE FROM users WHERE id = ? AND login = ? ")){
			for (User user: users) {
				statement.setInt(1, user.getId());
				statement.setString(2, user.getLogin());
				statement.executeUpdate();
			}
			return true;
		}catch (SQLException e){
			e.printStackTrace();
			throw new DBException("exception in deleteUsers", e);
		}

	}

	public User getUser(String login) throws DBException {
		Connection connection =  null;
		PreparedStatement statement = null;
		ResultSet rs = null;
		try{
			connection = DriverManager.getConnection(URL);
			statement = connection.prepareStatement("SELECT * FROM users WHERE login = ? ");

			statement.setString(1,login);

		    rs = statement.executeQuery();
		    User user = null;
		    if(rs.next()) {
		    	user = new User();
				user.setId(rs.getInt("id"));
				user.setLogin(rs.getString("login"));
			}
			return user;
		}catch (SQLException e){
			e.printStackTrace();
			throw new DBException("exception in getUser", e);
		} finally {
			helperClose(rs);
			helperClose(statement);
			helperClose(connection);
		}

	}

	public Team getTeam(String name) throws DBException {
		Connection connection =  null;
		PreparedStatement statement = null;
		ResultSet rs = null;
		try{
			connection = DriverManager.getConnection(URL);
			statement = connection.prepareStatement("SELECT * FROM teams WHERE name = ? ");

			statement.setString(1,name);

			rs = statement.executeQuery();
			Team team = null;
			if(rs.next()) {
				team = new Team();
				team.setId(rs.getInt("id"));
				team.setName(rs.getString("name"));
			}
			return team;
		}catch (SQLException e){
			e.printStackTrace();
			throw new DBException("exception in getTeam", e);
		} finally {
			helperClose(rs);
			helperClose(statement);
			helperClose(connection);
		}
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> teams = new ArrayList<>();
		try(Connection connection = DriverManager.getConnection(URL);
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery("SELECT * FROM teams ");){
			while (rs.next()){
				Team team = new Team();
				team.setId(rs.getInt("id"));
				team.setName(rs.getString("name"));
				teams.add(team);
			}

		} catch (SQLException e){
			e.printStackTrace();
		}
		return teams;
	}

	public boolean insertTeam(Team team) throws DBException {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet rs = null;
		try {
			connection = DriverManager.getConnection(URL);
			statement = connection.prepareStatement("INSERT INTO teams VALUES (DEFAULT, ?)", Statement.RETURN_GENERATED_KEYS);

			statement.setString(1, team.getName());

			int count = statement.executeUpdate();
			if(count > 0) {
				rs = statement.getGeneratedKeys();
				if(rs.next()){
					team.setId(rs.getInt(1));
					return true;
				}
			}
			return false;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("exception in insertTeam", e);
		} finally {
			helperClose(rs);
			helperClose(statement);
			helperClose(connection);
		}
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		Connection connection = null;
		PreparedStatement statement = null;
		try{
			connection = DriverManager.getConnection(URL);
			connection.setAutoCommit(false);
			statement = connection.prepareStatement("INSERT INTO users_teams (user_id, team_id) VALUES (?, ?)");
			for (Team team : teams) {
			statement.setInt(1, user.getId());
			statement.setInt(2, team.getId());
			statement.executeUpdate();
			}
			connection.commit();
			return true;
		}catch (SQLException e){
		 e.printStackTrace();
		 if(connection != null){
		 	try {
		 		connection.rollback();
				throw new DBException("exception in setTeamsForUser", e);
			} catch (SQLException e1){
				throw new DBException("exception in setTeamsForUser", e);
			}
		 }

		} finally {
			helperClose(connection);
			helperClose(statement);
		}
		return false;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> teams = new ArrayList<>();
		List<Integer> teamsId = new ArrayList<>();

		try (Connection connection = DriverManager.getConnection(URL);) {
			PreparedStatement findAllTeams = connection.prepareStatement(
					"SELECT * FROM users_teams WHERE user_id=?  ");
			findAllTeams.setInt(1, user.getId());

			ResultSet resultSet = findAllTeams.executeQuery();

			while (resultSet.next()) {
				teamsId.add(resultSet.getInt("team_id"));
			}
		} catch (SQLException e) {
			throw new DBException("Cannot get user teams", e.getCause());
		}

		for (Integer id : teamsId) {
			try (Connection connection = DriverManager.getConnection(URL);) {
				PreparedStatement findAllTeams = connection.prepareStatement(
						"SELECT * FROM teams WHERE id=? ");
				findAllTeams.setInt(1, id);

				ResultSet resultSet = findAllTeams.executeQuery();
				while (resultSet.next()) {
					Team team = new Team();
					team.setId(id);
					team.setName(resultSet.getString("name"));
					teams.add(team);
				}
			} catch (SQLException e) {
				throw new DBException("Cannot get user teams", e.getCause());
			}
		}
		return teams;
	}

	public boolean deleteTeam(Team team) throws DBException {
		try(Connection connection = DriverManager.getConnection(URL);
			PreparedStatement statement = connection.prepareStatement(" DELETE FROM teams WHERE id = ? AND name = ? ");){
			statement.setInt(1, team.getId());
			statement.setString(2, team.getName());
			return statement.executeUpdate() > 0;
		}catch (SQLException e){
			e.printStackTrace();
			throw new DBException("exception in deleteTeam", e);
		}

	}

	public boolean updateTeam(Team team) throws DBException {

		try (Connection connection = DriverManager.getConnection(URL);) {
			PreparedStatement updateTeamStatement = connection.prepareStatement(
					"UPDATE teams SET name =? WHERE id=?");

			updateTeamStatement.setString(1, team.getName());
			updateTeamStatement.setInt(2, team.getId());

			updateTeamStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DBException("Cannot update team", e.getCause());
		}
		return true;
	}
	private Properties createProperties() {
		Properties appProps = new Properties();
		try (var input = new FileInputStream(APP_CONFIG_PATH)) {
			appProps.load(input);
		} catch (IOException exception) {
			exception.printStackTrace();
		}
		return appProps;
	}

}
